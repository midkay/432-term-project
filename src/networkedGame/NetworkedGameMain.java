package networkedGame;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.awt.Font;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.tiled.TiledMap;


/*
 * Java Swing Imports
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**/

public class NetworkedGameMain extends BasicGame
{
	// ---  Constants  ---
	protected static final int SCREEN_WIDTH = 1280;
	protected static final int SCREEN_HEIGHT = 800;
	protected static final String GAME_NAME = "UWB CSS DM!";
	protected static final String resLocation = "res/";

	private static final int DEATH_TIME = 3000;
	private static final int FIRE_DELAY = 580;
	private static final int KILL_DISPLAY_TIMEOUT = 840;
	private static final int CHAT_DISPLAY_TIMEOUT = 5000;
	
	// ---  Game state variables  ---
	private static Font chatFont;
	private static TrueTypeFont chatTTF;
	protected static NetworkedGameData gameData;
	protected static NetworkedGamePlayer player;
	protected final float moveSpeedV = 0.36f;
	protected final float moveSpeedH = 0.3f;
	protected final float gravSpeed = 0.36f;
	protected static float worldX = 0;
	protected static float worldY = 0;
	protected static int mapHeightInPixels = 0;
	protected static int mapWidthInPixels = 0;
	private int uptime = 0;
	private int fireTimeout = 0;
	private int deathTimeout = 0;
	private int killMsgTimer = KILL_DISPLAY_TIMEOUT;
	protected static ArrayList<NetworkedGamePlayer> playerList;
    protected static ArrayList<NetworkedGameProjectile> projectileList;
	private boolean dead = false;
	
	private boolean atTitleScreen = true;
	private boolean atGameplayIntro = false;

	private static float jumpSpeed = 0;
	private static boolean jumping = false;
	private static boolean jumpKeyBeingHeld = false;

	// ---  In-game chat related  ---
    protected static LinkedList<String> chatMessages;
	private static TextField chatBox;
	private static int chatDisplayTimer = 0;
	
	// ---  External resources  ---
	protected static Image floor;
	protected static Image logo;
	protected static Image deathMessage;
	protected static Image killMessage;
	
	// ---  Title screen related  ---
	private static Image titleScreen;
	private static Image buttonConnect;
	private static Image buttonHost;
	private static Image buttonHotJoin;
	private static Image buttonRefresh;
	private static Image cursorImg;
	private static TextField TF_hostName;
	private static TextField TF_hostPort;
	private static TextField TF_lobbyName;
	private static TextField TF_lobbyPort;
	private static TextField TF_userName;
	private static Font titleFont;
	private static TrueTypeFont titleTTF;
	private static int titleMouseAreas = 4;	// number of areas on the title screen that show mouse hover
	private static MouseOverArea[] areas = new MouseOverArea[titleMouseAreas];
	private static String titleScreenMsg;

	// ---  Gameplay intro related  ---
	private static float GPI_bgOpacity = 1.0f;
	private static Image GPI_bg;
	private static Image GPI_playingAs;
	private static boolean GPI_showHeader = false;
	private static boolean GPI_showChar = false;
	private static boolean GPI_showTimer = false;
	private static Font GPI_font;
	private static TrueTypeFont GPI_ttf;

	// ---  Networking-related  ---
	protected static Socket clientSocket;
	protected static PrintWriter out;
	protected static BufferedReader in;
    private static final int updatesPerSec = 25;
	private static final int updateFrequency = 1000 / updatesPerSec; // 1000ms in 1 second
	private int deltaSinceLastUpdate = 0;
	private static int playerID;
	private static String[] listOfGames;	//The list of games we can connect to from the server
	private JFrame frameOfGames;			//My frame for displaying games
	
	// --- Hosts and Ports
	private static final String localhost = "localhost";
	private static final String hotJoinServer = "uw1-320-14.uwb.edu";
	private int serverPort = 10668;
	private int hotJoinPort	= 2780;
	
	// --- Talking to Server Related
	private String[] ClientToServerRolePlayLines = {"REQUEST_LIST_OF_GAMES", "RESERVE_THIS_USERNAME", "RESERVE_THIS_ROOM"};				//The lines I use to talk to the server
	private String ServerDelimit = ";;";																	//Shortcut delimiter for quick structured access
	private String[] ServerToClientRolePlayLines = {"ACK", "NAK"};											//The lines I listen from server for

	public NetworkedGameMain()
	{
		super(GAME_NAME);
	}

	public NetworkedGameMain(String title)
	{
		super(title);
	}

	// -- INIT -----------------------
	// All pre-game initialization happens here
	public void init(GameContainer gc) throws SlickException
	{
		gameData = new NetworkedGameData();
		floor = new Image(resLocation + "floor.png");
		logo = new Image(resLocation + "logo_32.png");
		deathMessage = new Image(resLocation + "death_message.png");
		killMessage = new Image(resLocation + "kill_message.png");
		GPI_bg = new Image(resLocation + "bluebg.png");
		GPI_playingAs = new Image(resLocation + "playing_as.png");
		GPI_font = new Font("Courier New", 1, 48);
		GPI_ttf = new TrueTypeFont(GPI_font, true);
		playerList = new ArrayList<NetworkedGamePlayer>();
		projectileList = new ArrayList<NetworkedGameProjectile>();
		playerID = (int) (Math.random() * 999999);
		chatMessages = new LinkedList<String>();
		mapWidthInPixels = NetworkedGameData.map.getWidth() * NetworkedGameData.TILE_SIZE;
		mapHeightInPixels = NetworkedGameData.map.getHeight() * NetworkedGameData.TILE_SIZE;
		
		// instantiate client's character
		int charID = (int) (Math.random() * gameData.getNumberOfCharacterTypes());
		player = new NetworkedGamePlayer(0, 0, charID, gameData.getCharWidth(charID), gameData.getCharHeight(charID), true);
		placePlayerRandomly(player);
		playerList.add(player);
		
		// other initialization
		// initNetworkCode();
		initTitleScreen(gc);
		
		// set up chat box (for once we're ingame)
		chatFont = new Font("Courier New", 0, 14);
		chatTTF = new TrueTypeFont(chatFont, true);
		chatBox = new TextField(gc, chatTTF, 2, SCREEN_HEIGHT-22, 320, 20, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				if(chatBox.getText() == "")
				{
					chatBox.deactivate();
				}
				else
				{
					String msg = "<" + player.getUsername() + "> " + chatBox.getText();
					chatBox.setText("");
					chatDisplayTimer = 0;

					// package & send chat message to server
					JSONObject json = new JSONObject();
					json.put("chatMsg", msg);
					out.println(json);
				}
			}
		});
		chatBox.setBorderColor(Color.green);
		chatBox.setTextColor(new Color(224,224,224));
	}

	// -- UPDATE ---------------------
	// All internal game logic (e.g. movement, calculation) happens here
	public void update(GameContainer gc, int delta) throws SlickException
	{
		// ESC quits game
		Input input = gc.getInput();
		if (input.isKeyDown(Input.KEY_ESCAPE))
			System.exit(0);
		
		if(delta > 30)
		{
			delta = 30; // cap this to some sane amount
		}

		// we have separate update logic for title screen, gameplay intro, and gameplay
		if(atTitleScreen)
		{
			uptime += delta;
		}
		else if(atGameplayIntro)
		{
			uptime += delta;
			
			if(uptime < 1000)
			{
				GPI_bgOpacity = 1 - ((float) uptime / 2000);
			}
			else if(uptime < 2000)
			{
				GPI_showHeader = true;
			}
			else if(uptime < 3000)
			{
				GPI_showChar = true;
			}
			else if(uptime < 6000)
			{
				GPI_showTimer = true;
				//atGameplayIntro = false; // debug: skip ahead to main game
			}
			else // after 6 sec...
			{
				atGameplayIntro = false; // move onto main game
			}
		}
		else if(dead) // in limbo
		{
			deathTimeout -= delta;

			if(deathTimeout < 0)
			{
				// time to not be dead anymore
				placePlayerRandomly(player);
				dead = false;
			}
		}
		else
		{
			// update various timers and counters
			uptime += delta;
			deltaSinceLastUpdate += delta;

			fireTimeout -= delta;
			if(fireTimeout < 0)
				fireTimeout = 0;

			if(killMsgTimer < KILL_DISPLAY_TIMEOUT)
				killMsgTimer += delta;

			if(chatDisplayTimer < CHAT_DISPLAY_TIMEOUT)
				chatDisplayTimer += delta;

			Image playerImg = gameData.getCharFacingRight(player.charID); // get img of player

			player.moveY(gravSpeed * delta); // fall player down: gravity
	
			// ---  Player movement code  ---
			// We only want to handle any of this if player isn't chatting
			if(!chatBox.hasFocus())
			{
				if (input.isKeyDown(Input.KEY_A)) // move left
				{
					player.facingRight = false;
					player.moveX(-moveSpeedH * delta);
				}
				if (input.isKeyDown(Input.KEY_D)) // move right
				{
					player.facingRight = true;
					player.moveX(moveSpeedH * delta);
				}
				if (input.isKeyDown(Input.KEY_S)) // move down
				{
					if(player.getY() + playerImg.getHeight() < SCREEN_HEIGHT)
						player.moveY(moveSpeedV * delta);
				}
				if (input.isKeyDown(Input.KEY_W)) // move up
				{
					if(!jumping && !jumpKeyBeingHeld) {
						jumping = true;
						jumpKeyBeingHeld = true;
						jumpSpeed = (float) ((moveSpeedV + gravSpeed) * 1.5);
					}
				}
				if(!input.isKeyDown(Input.KEY_W))
				{
					jumpKeyBeingHeld = false;
				}
				if (input.isKeyDown(Input.KEY_SPACE) && fireTimeout == 0) // fire projectile!
				{
					int spawnX = (int) player.getX();
					int spawnY = (int) player.getY() + player.height/2;
					int projectileID = (int) (Math.random() * 999999);
					
					// spawn projectile on appropriate side of player
					if(player.facingRight)
						spawnX += player.width;
					else
						spawnX -= gameData.laserList.get(0).getWidth();
					
					//NetworkedGameProjectile tmp = new NetworkedGameProjectile(spawnX, spawnY, player.facingRight);
					//projectileList.add(tmp);
					fireTimeout = FIRE_DELAY;
					
	        		// Tell the server about this fired projectile
					JSONObject tmpObj = new JSONObject();
					tmpObj.put("projectileID", new Integer( projectileID ));
					tmpObj.put("posX", new Float(spawnX));
					tmpObj.put("posY", new Float(spawnY));
					tmpObj.put("travelingRight", new Boolean(player.facingRight));
					tmpObj.put("firedBy", new Integer(playerID));
					//System.out.println(tmpObj);
					out.println(tmpObj);
				}
			}
			
			// jumping calculations
			if(jumping) {
				player.moveY(-jumpSpeed * delta * (float) 1.8); // move up if jumping
				jumpSpeed *= 1 - (0.0045 * delta); // scale down jumping speed				
			}
			
			recomputeWorldPos();
			
			player.tick(); // tick ourselves
			
			// --------  Network code  ----------
	        String fromServer = null;
	        try {
	        	// Any data awaiting to be received?
	        	if(in.ready())
	        	{
	            	// There is; read and process it
					if ((fromServer = in.readLine()) != null)
					{
		    		    JSONParser parser = new JSONParser();
					    //System.out.println("Server: " + fromServer);
					    
				        Object obj = parser.parse(fromServer);
				        JSONArray array = (JSONArray) obj;
				        //System.out.println("Array size: " + array.size());
				        
				        // reset our player list; it's about to get repopulated
				        playerList.clear();
				        playerList.add(player);
				        projectileList.clear(); // this too
				        
				        // handle data for each entity individually
				        for(int i = 0; i <array.size(); i++)
				        {
				            JSONObject obj2 = (JSONObject) array.get(i);
				        	//System.out.println("-- entity " + i + " json data: " + array.get(0));
				            
				            if(obj2.get("playerID") != null)
				            {
				            	// this is player data
								// read individual values as strings
								String username = obj2.get("username").toString();
								String posX_str = obj2.get("posX").toString();
								String posY_str = obj2.get("posY").toString();
								String charID_str = obj2.get("charID").toString();
								String facingRight_str = obj2.get("facingRight").toString();
								String playerID_str = obj2.get("playerID").toString();
								String kills_str = obj2.get("k").toString();
								String deaths_str = obj2.get("d").toString();
								
								// parse out raw values as needed
								float posX = Float.parseFloat(posX_str);
								float posY = Float.parseFloat(posY_str);
								int charID = Integer.parseInt(charID_str);
								boolean facingRight = Boolean.parseBoolean(facingRight_str);
								int tmpPlayerID = Integer.parseInt(playerID_str);
								int kills = Integer.parseInt(kills_str);
								int deaths = Integer.parseInt(deaths_str);
								
								if(tmpPlayerID == playerID)
								{
									// this is our own data, we only use a bit of it
									player.kills = kills;
									player.deaths = deaths;
									continue;
								}
								else
								{
									//System.out.println("adding new player data");
									// this is other-player data, let's draw them to the screen
									NetworkedGamePlayer tmp = new NetworkedGamePlayer((int) posX, (int) posY, charID, facingRight);
									tmp.kills = kills;
									tmp.deaths = deaths;
									tmp.setUsername(username);
									playerList.add(tmp);
								}
				            }
				            else if(obj2.get("projectileID") != null)
				            {
				            	// this is a projectile
				            	//System.out.print("Got data on projectile " + obj2.get("projectileID").toString());
								String posX_str = obj2.get("posX").toString();
								String posY_str = obj2.get("posY").toString();
								String travelingRight_str = obj2.get("travelingRight").toString();
								// String projectileID_str = obj2.get("projectileID").toString();
								
								// parse out raw values as needed
								float posX = Float.parseFloat(posX_str);
								float posY = Float.parseFloat(posY_str);
								boolean travelingRight = Boolean.parseBoolean(travelingRight_str);
								// int tmpProjectileID = Integer.parseInt(projectileID_str);

								// System.out.println(" (posX = " + posX + ", posY = " + posY + ")");
								
								NetworkedGameProjectile tmp = new NetworkedGameProjectile((int) posX, (int) posY, travelingRight);
								projectileList.add(tmp);
				            }
				            else if(obj2.get("death") != null)
				            {
				            	// this is a death message; grab its values
				            	System.out.print("Someone died ");
				            	String killed = obj2.get("death").toString();
				            	String killer = obj2.get("by").toString();
				            	int killedID = Integer.parseInt(killed);
				            	int killerID = Integer.parseInt(killer);
				            	
				            	// was it us that died?
				            	if(playerID == killedID)
				            	{
				            		// yes
				            		System.out.println("(us)");
				            		dead = true;
				            		deathTimeout = DEATH_TIME;
				            	}
				            	else
				            	{
				            		// no; but was it us that killed someone?
				            		System.out.println("(not us)");
				            		if(playerID == killerID)
				            		{
				            			// we did the killing; display a message
					            		killMsgTimer = 0;
				            		}
				            	}
				            }
				            else if(obj2.get("chatMsg") != null)
				            {
				            	// this is a chat message; add it to our array
				            	String msg = obj2.get("chatMsg").toString();
				            	System.out.println("Got chat message: " + msg);
				            	chatMessages.addFirst(msg);
				            	
				            	// also ensure it's visible
				            	chatDisplayTimer = 0;
				            }
				        }
					}
	        	}
	        	
	        	// Let's also send our data to the server (if we're due for an update)
	        	if(deltaSinceLastUpdate > updateFrequency)
	        	{
	        		// Package and send our player stats
					JSONObject tmpObj = new JSONObject();
					tmpObj.put("playerID", new Integer( playerID ));
					tmpObj.put("username", player.getUsername());
					tmpObj.put("charID", new Integer( player.getCharID() ));
					tmpObj.put("posX", new Float( player.getX() ));
					tmpObj.put("posY", new Float( player.getY() ));
					tmpObj.put("facingRight", new Boolean(player.facingRight));
					tmpObj.put("k", new Integer(player.kills));
					tmpObj.put("d", new Integer(player.deaths));
					//System.out.println(tmpObj); 
					out.println(tmpObj);

		        	deltaSinceLastUpdate -= updateFrequency;
	        	}
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}
		}
	}

	// -- RENDER -----------------------------------
	// All game rendering code (e.g. drawing images, text) happens in here.
	public void render(GameContainer gc, Graphics g) throws SlickException
	{
		if(atTitleScreen)
		{
			titleScreen.draw(0, 0);
			
			for (int i = 0; i < areas.length; i++)
			{
				areas[i].render(gc, g);
			}
			TF_hostName.render(gc, g);
			TF_hostPort.render(gc, g);
			TF_lobbyName.render(gc, g);
			TF_lobbyPort.render(gc, g);
			TF_userName.render(gc, g);
			
			// render a continuous loop of characters
			int whichChar = (uptime / 1350) % gameData.getNumberOfCharacterTypes();
			Image titleScreenChar = gameData.getCharFacingLeft(whichChar).getScaledCopy(1.29f);
			titleScreenChar.setFilter(Image.FILTER_NEAREST); // disable interpolation
			titleScreenChar.draw(1150 - titleScreenChar.getWidth() / 2, 168 - titleScreenChar.getHeight());
			
			g.setFont(titleTTF);
			g.drawString(titleScreenMsg, 40, 760);
		}
		else if(atGameplayIntro)
		{
			// introduce our characters
			GPI_bg.draw(0, 0);
			GPI_bg.setImageColor(1f, 1f, 1f, GPI_bgOpacity);
			
			g.setFont(GPI_ttf);
			
			if(GPI_showHeader)
				GPI_playingAs.draw(100, 50);
			
			if(GPI_showChar)
			{
				int charID = playerList.get(0).getCharID();
				Image charImg = gameData.getCharFacingRight(charID);
				charImg.setFilter(Image.FILTER_NEAREST);
				charImg = charImg.getScaledCopy(3.0f);
				charImg.draw(SCREEN_WIDTH/2 - charImg.getWidth()/2f, 230);
				
				String name = gameData.getCharName(charID);
		        int textWidth = GPI_ttf.getWidth(name);
				g.drawString(name, (int) (SCREEN_WIDTH/2 -  textWidth/2f), 240 + charImg.getHeight());
			}
			
			if(GPI_showTimer)
			{
				int secs = 6 - (uptime/1000);
				String text = secs + "...";
		        int textWidth = GPI_ttf.getWidth(text);
				g.drawString(text, (int) (SCREEN_WIDTH/2 - textWidth/2f), 700);
			}
		}
		else if(dead)
		{
			GPI_bg.draw(0, 0);
			GPI_bg.setImageColor(1f, 1f, 1f, GPI_bgOpacity);
			deathMessage.draw(SCREEN_WIDTH/2-deathMessage.getWidth()/2, SCREEN_HEIGHT/4);

			g.setFont(GPI_ttf);
			g.drawString("Please wait " + (float) (deathTimeout/10)/100 + " sec...",
					SCREEN_WIDTH/2-g.getFont().getWidth("Please wait x.x sec...")/2, SCREEN_HEIGHT/2+80);
		}
		else
		{
			// draw the world map
			drawLayer(NetworkedGameData.map.getLayerIndex("world"));
	
			// draw all players
			for(int i = 0; i < playerList.size(); i++)
			{
				// grab the ith player
				NetworkedGamePlayer user = playerList.get(i);
				int x = (int) (user.getX() - worldX);
				int y = (int) (user.getY() - worldY);

				// draw them
				if(user.facingRight)
					gameData.getCharFacingRight(user.getCharID()).draw(x, y);
				else
					gameData.getCharFacingLeft(user.getCharID()).draw(x, y);
				
				// also draw a username, player stats, and character name 
				g.drawString(user.getUsername(), x, y - 35);
				g.drawString("(" + gameData.getCharName(user.getCharID()) + ")", x, y - 20);
			}
			
			// draw any+all projectiles
			for(int i = 0; i < projectileList.size(); i++)
			{
				// pick a random laser texture
				NetworkedGameProjectile tmp = projectileList.get(i);
				int whichLaser = (int) (Math.random() * gameData.getNumLaserTypes());
				gameData.getLaser(whichLaser).draw(tmp.posX - worldX, tmp.posY - worldY);
			}
			
			// draw map overlays
			drawLayer(gameData.map.getLayerIndex("overlay"));

			// ---  draw a scoreboard  ---
			// backdrop first
			int textWidth = g.getFont().getWidth("+--[ Player list and scores ]--+");
			Rectangle r = new Rectangle(6, 6, textWidth + 8, 40 + playerList.size() * 18 + 8);
			Color colorAlpha = new Color(0f, 0f, 0f, 0.32f); // transparent black box
			g.setColor(colorAlpha);
			g.fill(r);
			g.setColor(Color.white);
			
			// now actually draw player/score list
			g.drawString("+--[ Player list and scores ]--+", 10, 34);
			for(int p = 0; p < playerList.size(); p++)
			{
				// loop over all players
				NetworkedGamePlayer user = playerList.get(p);
				g.drawString(" - " + user.getUsername() + " (K:" + user.getKills()
						+ " D:" + user.getDeaths() + ")", 10, 52 + p*18);
			}
			
			// draw chatbox / messages
			chatBox.render(gc, g);			
			if(chatDisplayTimer < CHAT_DISPLAY_TIMEOUT && chatMessages.size() > 0)
			{
				int ypos = SCREEN_HEIGHT - 40;
				int chatLogHeight = 0;

				for(int i = 0; i < chatMessages.size(); i++)
				{
					String msg = chatMessages.get(i);
					g.drawString(msg, 2, ypos);

					chatLogHeight += g.getFont().getHeight(msg);
					ypos -= 20;
				}
			}
			
			// draw a kill message if it's relevant
			if(killMsgTimer < KILL_DISPLAY_TIMEOUT)
			{
				killMessage.draw(SCREEN_WIDTH/2 - killMessage.getWidth()/2, SCREEN_HEIGHT/2 - killMessage.getHeight()/2);
			}
			
			// draw our logo in the bottom-right
			logo.draw(SCREEN_WIDTH-212, SCREEN_HEIGHT-32);
		}
	}
	
	public static void main(String[] args) throws SlickException
	{
		AppGameContainer app = new AppGameContainer(new NetworkedGameMain(GAME_NAME));
		
		app.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
		app.setUpdateOnlyWhenVisible(false);
		app.setAlwaysRender(true);
		app.setTargetFrameRate(200);
		//app.setFullscreen(true);
		app.start();
	}

	// sets jumping speed to 0 and optionally re-allows jumping
	protected static void stopJump(boolean landed)
	{
		jumpSpeed = 0;
		if(landed)
			jumping = false;
	}
	
	// place the client's player somewhere random BUT VALID within the world
	private void placePlayerRandomly(NetworkedGamePlayer p) throws SlickException
	{
		int xpos, ypos;
		
		// repeatedly generate random x/y coordinates until valid
		do {
			worldX = 0;
			worldY = 0;
			xpos = (int) (Math.random() * mapWidthInPixels);
			ypos = (int) (Math.random() * mapHeightInPixels);
		}
		while(player.checkCollision((float) xpos, (float) ypos) == NetworkedGameData.BLOCK_SOLID);
		
		p.setPos(xpos, ypos);
		
		// coords are valid, but player may be floating mid-air; fall them down to the ground
		for(int j = 0; j < 50; j++)
			p.moveY(20);

		worldY = p.getY() - (SCREEN_HEIGHT / 2);
	}
	
	/**
	 * Initializes graphical components for game title screen.
	 * @param gc
	 * @throws SlickException
	 */
	private void initTitleScreen(GameContainer gc) throws SlickException 
	{
		// initialize images, cursor + font
		titleScreen = new Image(resLocation + "title_screen.png");
		buttonConnect = new Image(resLocation + "button_connect.png");
		buttonHost = new Image(resLocation + "button_host.png");
		buttonHotJoin = new Image(resLocation + "button_hotjoin.png");
		buttonRefresh = new Image(resLocation + "button_refresh.png");
		cursorImg = new Image(resLocation + "cursor.png");
		titleFont = new Font("Courier New", 0, 24);
		titleTTF = new TrueTypeFont(titleFont, true);
		titleScreenMsg = "Choose to host or join a game.";
		gc.setMouseCursor(cursorImg, 0, 0);

		// create client "Host name" text field
		TF_hostName = new TextField(gc, titleTTF, 325, 264, 500, 35, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				titleScreenMsg = "Entered: " + TF_hostName.getText();
			}
		});
		TF_hostName.setBorderColor(Color.cyan);
		
		// create client "Port" text field
		TF_hostPort = new TextField(gc, titleTTF, 325, 324, 140, 35, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				titleScreenMsg = "Entered: " + TF_hostPort.getText();
			}
		});
		TF_hostPort.setBorderColor(Color.cyan);
		
		// create client "Username" text field
		TF_userName = new TextField(gc, titleTTF, 325, 384, 320, 35, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				titleScreenMsg = "Entered: " + TF_userName.getText();
			}
		});
		TF_userName.setBorderColor(Color.cyan);
		
		// create server "Lobby name" text field
		TF_lobbyName = new TextField(gc, titleTTF, 325, 580, 500, 35, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				titleScreenMsg = "Entered: " + TF_lobbyName.getText();
			}
		});
		TF_lobbyName.setBorderColor(Color.orange);

		// create server "Lobby port" text field
		TF_lobbyPort = new TextField(gc, titleTTF, 325, 640, 140, 35, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				titleScreenMsg = "Entered: " + TF_lobbyPort.getText();
			}
		});
		TF_lobbyPort.setBorderColor(Color.orange);
		
		// create client "Connect" button
				areas[0] = new MouseOverArea(gc, buttonConnect, 500, 314, 183, 54, new ComponentListener() {
					public void componentActivated(AbstractComponent source) {
						if(TF_hostName.getText() == "")
							titleScreenMsg = "Invalid hostname!";
						else if(TF_hostPort.getText() == "")
							titleScreenMsg = "Invalid host port!";
						else if(!TF_hostPort.getText().matches("\\d+"))
								titleScreenMsg = "Invalid port value!";
						else if(TF_userName.getText() == "")
							titleScreenMsg = "Please enter a username!";
						else
						{
							// hostname and port fields are filled out; attempt to connect with them
							titleScreenMsg = "Client connect button pressed! Connect to " + TF_hostName.getText() + ":" + TF_hostPort.getText();
							/*if(player.getUsername() == null)
							{
								player.setUsername(TF_userName.getText());
							}*/
							if(joinGame(TF_hostName.getText(), Integer.parseInt(TF_hostPort.getText())))
							{
								player.setUsername(TF_userName.getText());
								atTitleScreen = false;
								atGameplayIntro = true;
								uptime = 0;
							}
							else
								titleScreenMsg = "Couldn't find " + TF_hostName.getText() + ":" + TF_hostPort.getText();
						}
					}
				});
				areas[0].setNormalColor(new Color(1, 1, 1, 0.8f));
				areas[0].setMouseOverColor(new Color(1, 1, 1, 0.9f));		

		// create server "Host" button
		areas[1] = new MouseOverArea(gc, buttonHost, 500, 630, 183, 54, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				if(TF_lobbyName.getText() == "")
					titleScreenMsg = "Invalid lobby name!";
				else if(TF_lobbyPort.getText() == "" || !TF_lobbyPort.getText().matches("\\d+"))
					titleScreenMsg = "Invalid lobby port!";
				else
				{
					titleScreenMsg = "Server host button pressed! Attempting to set up \"" + TF_lobbyName.getText() + "\" on port " + TF_lobbyPort.getText();
					String tmpHostName = hotJoinServer;
					if(TF_hostName.getText() != "")
						tmpHostName = TF_hostName.getText();
					if(reserveGameRoom(tmpHostName, Integer.parseInt(TF_lobbyPort.getText()), TF_lobbyName.getText()))
					{
						titleScreenMsg = "New room created. Connect to the room by typing its port number and a user name into the connection fields";
					}
					else
					{
						titleScreenMsg = "Could not set up the requested room.";
					}
				}
			}
		});
		areas[1].setNormalColor(new Color(1, 1, 1, 0.8f));
		areas[1].setMouseOverColor(new Color(1, 1, 1, 0.9f));

		// create "Hotjoin" button
		areas[2] = new MouseOverArea(gc, buttonHotJoin, 1015, 720, 183, 54, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				if(TF_userName.getText() == "")
				{
					titleScreenMsg = "Please enter a username!";
				}
				else if(hotJoin())
				{
					player.setUsername(TF_userName.getText());
					atTitleScreen = false;
					atGameplayIntro = true;
					uptime = 0;
					
					try {
						clientSocket.setTcpNoDelay(true);
					} catch (SocketException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else
				{
					titleScreenMsg = "Couldn't hotjoin :\\";
				}
			}
		});
		areas[2].setNormalColor(new Color(1, 1, 1, 0.8f));
		areas[2].setMouseOverColor(new Color(1, 1, 1, 0.9f));
		
		// create "Refresh" button
		areas[3] = new MouseOverArea(gc, buttonRefresh, 1015, 620, 183, 54, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				/*INIT variables*/
				titleScreenMsg = "Refreshing List of Games...";
				
				String[] serversToAttempt = {hotJoinServer, localhost};
				//CHECK if user has specified a hostname
				if(TF_hostName.getText() != "")
				{
					serversToAttempt[0] = TF_hostName.getText();
				}
					
				String result = null;
				int i = 0;
				
				/*ATTEMPT to get list of games from server*/
				while(result == null)
				{
					result = refreshGameList(serversToAttempt[i]);
					
					/*DISPLAY list of games from server*/
					if(result != null)
					{
						listOfGames = result.split(ServerDelimit);
						for(int j = 0; j < listOfGames.length; j++)
							System.out.println(listOfGames[j]);														//Debug Output
						displayListOfGames();											//150602 BEN: Display game listing in separate window for now
						break;
					}
					i++;
					if(i == serversToAttempt.length)
					{
						System.err.println("ERR: Cannot reach any servers.");
						break;
					}
				}
				
				//System.out.println(result);																					//Debug output
			}
		});
		areas[3].setNormalColor(new Color(1, 1, 1, 0.8f));
		areas[3].setMouseOverColor(new Color(1, 1, 1, 0.9f));
	}

	// ---  World scrolling code, to move camera with player as appropriate  ---
	private void recomputeWorldPos()
	{
		// move the world view left if player's global x coordinate nears the right edge
		if(player.getX() - worldX > (.7 * SCREEN_WIDTH))
			worldX = (float) (player.getX() - (.7 * SCREEN_WIDTH));
		// move the world view right if player nears left edge
		else if(player.getX() - worldX < (.3 * SCREEN_WIDTH))
		{
			worldX = (float) (player.getX() - (.3 * SCREEN_WIDTH));
			if(worldX < 0)
				worldX = 0;
		}
		// move the world view down if player nears the bottom edge
		if(player.getY() - worldY > (.65 * SCREEN_HEIGHT))
			worldY = (float) (player.getY() - (.65 * SCREEN_HEIGHT));
		// move the world view up if player nears top edge
		if(player.getY() - worldY < (.3 * SCREEN_HEIGHT))
			worldY = (float) (player.getY() - (.3 * SCREEN_HEIGHT));
		
		// also, make sure camera is capped to reasonable bounds
		if(worldX < 0)
			worldX = 0;
		else if(worldX > mapWidthInPixels - SCREEN_WIDTH)
			worldX = mapWidthInPixels - SCREEN_WIDTH;
		if(worldY < 0)
			worldY = 0;
		else if(worldY > mapHeightInPixels - SCREEN_HEIGHT)
			worldY = mapHeightInPixels - SCREEN_HEIGHT;
	}
	
	// render the given layer, efficiently -- draw only the tiles we need drawn
	private void drawLayer(int layer)
	{
		/*Image tmp = null;
		
		for(int x = 0; x < SCREEN_WIDTH/NetworkedGameData.TILE_SIZE + 1; x++)
		{
			for(int y = 0; y < SCREEN_HEIGHT/NetworkedGameData.TILE_SIZE + 1; y++)
			{
	            tmp = NetworkedGameData.map.getTileImage(x, y, layer);
	            if(tmp != null)
		            tmp.draw(x*NetworkedGameData.map.getTileWidth() - worldX, y*NetworkedGameData.map.getTileHeight());
			}
		}*/
		
		int tileSize = NetworkedGameData.TILE_SIZE;
		
		gameData.map.render(-tileSize + (int) (-worldX % tileSize), -tileSize
				+ (int) (-worldY % tileSize), (int) (worldX / tileSize) - 1,
					(int) (worldY / tileSize) - 1, SCREEN_WIDTH / tileSize + 2,
						SCREEN_HEIGHT / tileSize + 2, layer, true);
	}
	
	/**
	 * @return	Boolean, can I hot join a game?
	 */
	private boolean hotJoin()
	{
		// attempt to join localhost, or else our default server
		if(joinGame(localhost, hotJoinPort))
			return true;
		else if(joinGame(hotJoinServer, hotJoinPort))
			return true;
		return false;	
	}
	
	/**
	 * @param hostName	: Host name of game
	 * @param portNumber: Port number of game
	 * @return	Boolean, can I join this game?
	 */
	private boolean joinGame(String hostName, int portNumber)
	{
		// join the game
		try {
            clientSocket = new Socket(hostName, portNumber);
            clientSocket.setTcpNoDelay(true);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
           
            // Connected, now submit data to server
            JSONObject initObj = new JSONObject();
            initObj.put("username", new String( player.getUsername() ));
            initObj.put("playerID", new Integer( playerID ));
			out.println(initObj);
	
            return true;
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            return false;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            return false;
        }
	}
	
	/**
	 * @pre		Server's port is on serverPort
	 * @param	Name, the name you want to register with the server
	 * @param hostName	: Host name of game
	 * @return	True if accepted, else return false
	 */
	private boolean registerUsername(String name, String hostName)
	{
		return true;
	}
	
	/**
	 * @pre		Server's port is on serverPort
	 * @param hostName	: Host name of game
	 * @param portNumber: Port number of game
	 * @return	ListOfGames, the list of games running on the server, NULL if empty
	 */
	private String refreshGameList(String hostName)
	{
		String listOfGames = null;
		
		//System.out.println("Checking host " + hostName);																//Debug output
		/*TRY connecting to the server on the server's listening port*/
		try {
            clientSocket = new Socket(hostName, serverPort);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
            
            out.println(ClientToServerRolePlayLines[0]);		//Request game list from server
            listOfGames = in.readLine();						//Read response from server
            System.out.println("Successfully checked host " + hostName + ": ");																	//Debug output
            
        } catch (UnknownHostException e) {
            //System.err.println("Don't know about host " + hostName);
        } catch (IOException e) {
           //System.err.println("Couldn't get I/O for the connection to " + hostName);
        }
		return listOfGames;				//Whether or not it's null, return
	}

	/**
	 * @param hostName		: Name of the server
	 * @param portNum		: Port number to open the new room
	 * @param roomName		: Name of the new room
	 * @return True or false, based on whether we could open our desired room or not
	 */
	private boolean reserveGameRoom(String hostName, int portNum, String roomName)
	{
		String response = "";
		
		/* TRY connecting to the server on the server's listening port */
		try {
            clientSocket = new Socket(hostName, serverPort);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
            
            out.println(ClientToServerRolePlayLines[2]);		//Request a room from the server
            out.println(portNum);
            out.println(roomName);
            response = in.readLine();							//Read response from server
            System.out.println(response);														//Debug Output
            
            /*ONLY return true if ACKed*/
            if(response == ServerToClientRolePlayLines[0])
            	return true;
            
        } catch (UnknownHostException e) {
            //System.err.println("Don't know about host " + hostName);
        } catch (IOException e) {
           //System.err.println("Couldn't get I/O for the connection to " + hostName);
        }
		return false;
	}
	
	/**
	 * Renders a list of games in a separate java window
	 * @pre listOfGames	:	The list of games we retrieved from the server that we save is not empty or null
	 */
	private void displayListOfGames()
	{
		//Remove previous versions
		if(frameOfGames != null)
			frameOfGames.dispose();
		
		/*Create a local panel with list of games as its text*/
		int defaultWidth = 300;
		int lineHeight = 20;
		JEditorPane paneOfGames = new JEditorPane();
		paneOfGames.setEditable(false);
		
		//CHECK if it's null
		if(listOfGames == null || listOfGames.length == 0)
			paneOfGames.setText("Unable to get list of games...");
		//FILL contents of panel with listOfGames
		else
		{
			String delimitedList = "";
			for(int i = 0; i < listOfGames.length; i++)
				delimitedList += listOfGames[i] + ("\r\n");
			paneOfGames.setText(delimitedList);
			Dimension d = new Dimension(defaultWidth, (lineHeight * listOfGames.length));
			paneOfGames.setPreferredSize(d);
		}
		
		/*CREATE a frame to hold and display the pane*/
		frameOfGames = new JFrame("List Of Games: ");
		frameOfGames.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameOfGames.add(paneOfGames);
		frameOfGames.pack();
		frameOfGames.setVisible(true);
	}
}