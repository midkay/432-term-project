package networkedGame;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.*;
import java.net.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
 
public class NetworkedGameServerThread extends Thread {
	public static int minPortNumber = 1024;			//The minimum port number we accept
    private Socket socket = null;
    private long timeout = 10000;			//timeout, close me on timeout
	private long curTime;
	private long prevTime;;
    private int portNumber = -1;
    String name = "";
    private int maxPlayers = 100;
    private int gameDataBufferSize = 2;
    private static final int updatesPerSec = 25;
	private static final int updateFrequency = 1000 / updatesPerSec; // 1000ms in 1 second
	private long timeLastSent = 0; // system time last packet was sent
    BlockingQueue<PlayerThread> myPlayers = new LinkedBlockingQueue<PlayerThread>(maxPlayers);  //my list of players
    BlockingQueue<String> gameData = new LinkedBlockingQueue<String>(gameDataBufferSize);		//for Jsocket's WriteTo
    BlockingQueue<String> clientData = new LinkedBlockingQueue<String>(maxPlayers);				//for Jsocket's ReadFrom
    
    // ---  Game data type stuff  ---
    private volatile Map<Integer, NetworkedGamePlayer> playerMap;
    private Map<Integer, NetworkedGameProjectile> projectileMap;
    private Map<Integer, Integer> queuedDeaths; // maps the killed to their killer
    private ArrayList<JSONObject> queuedChatMessages;
    
    //In house clean-up, by sharing a volatile version of player map with a helper thread
    private ThreadMapThreadCleanUp<NetworkedGameServerThread, PlayerThread, NetworkedGamePlayer> cleanUp;
    BlockingQueue<Integer> markedForRemove = new LinkedBlockingQueue<Integer>(maxPlayers);
 
    public NetworkedGameServerThread(Socket socket) throws SocketException {
        super("NetworkedGameServerThread");
        this.socket = socket;
        socket.setTcpNoDelay(true);

    	initData();
    }
    
    /**
     * @param portNumber	The port number to listen from, assigned from GameServer
     * @param name			Name of the channel
     */
    public NetworkedGameServerThread(int portNumber, String name)
    {
    	super("NetworkedGameServerThread");
    	this.portNumber = portNumber;
    	this.name = name;

    	initData();
    }
    
    /**
     * @param portNumber	The port number to listen from, assigned from GameServer
     * @param name			Name of the channel
     * @param timeout		Timeout for this particular game, decreed by Server
     * @param capacity		The maximum number of players supported, decreed by the Server
     */
    public NetworkedGameServerThread(int portNumber, String name, int capacity, int timeout)
    {
    	super("NetworkedGameServerThread");
    	this.portNumber = portNumber;
    	this.maxPlayers = capacity;
    	this.timeout = timeout;
    	this.name = name;

    	initData();
    }
    
    private void initData()
    {
		playerMap = new HashMap<Integer, NetworkedGamePlayer>();
		projectileMap = new HashMap<Integer, NetworkedGameProjectile>();
		queuedDeaths = new HashMap<Integer, Integer>();
		queuedChatMessages = new ArrayList<JSONObject>();
    }
    
    /**
     * @return port#, of the game channel listening thread, that listens for client connections
     */
    public int getPortNumber()
    {
    	return portNumber;
    }
    
    /**
     * @return Name, of the game channel
     */
    public String channelName()
    {
    	return name;
    }
    
    /**
     * @return Population, number of players in this game
     */
    public int population()
    {
    	return myPlayers.size();
    }
    
    /**
     * @return String of server, 'NAME, port#. Population'
     */
    public String status()
    {
    	String me;
    	me = (name + ", " + portNumber + ". Pop: " + population() + " out of " + maxPlayers);
    	//System.out.println("Called This, I am: " + me);												//Debug Output
    	return me;
    }
    
    /**
     * @return input, a string[] read from clientData
     * This is null if it's empty.
     */
    private String[] readFrom()
    {
    	String[] input = null;

    	if(clientData.isEmpty())
    		return input;
  
    	try {
    		input = new String[clientData.size()];
    		for(int i = 0; i < clientData.size(); i++)
    			input[i] = clientData.take();
        	clientData.clear();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	//Enable the player threads to write to me again
    	for(Iterator<PlayerThread> i = myPlayers.iterator(); i.hasNext();)
    	{
    		i.next().enable();
    	}

    	return input;
    }
    
    /**
     * @param content: Write this to gameData
     */
    private void writeTo(String content)
    {
    	if(gameData.remainingCapacity() == 0)
    		gameData.poll();			//remove head of queue
    	gameData.offer(content);		//add content to queue
    }
    
    /**
     * Cleans up the playerMap
     */
    private void cleanMapping()
    {
    	Integer[] input = new Integer[markedForRemove.size()];
		for(int i = 0; i < markedForRemove.size(); i++)
		{
			try {
				int victim = markedForRemove.take();
				if(playerMap.containsKey(victim))
				{
					playerMap.remove(victim);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
     
    public void run() {
    	NetworkedGameServerThreadHelper myHelper = new NetworkedGameServerThreadHelper(portNumber, this);
    	myHelper.start();
    	
        /* The game thread loop itself
         * Runs until timeout
         * Timeout counts when number of players = 0
         */
    	
    	cleanUp = new ThreadMapThreadCleanUp<NetworkedGameServerThread, PlayerThread, NetworkedGamePlayer>(this, playerMap);
    	cleanUp.start();
    	long timelimit = Long.MAX_VALUE;
    	boolean timeoutEnable = false;
    	curTime = System.currentTimeMillis();
    	prevTime = System.currentTimeMillis();
		Set<Integer> keys = null;
		
    	while(timelimit > curTime)
    	{
    		curTime = System.currentTimeMillis();
    		
    		/*Clean up my map*/
    		cleanMapping();
        	/*Read client data from JSocket*/
    		String[] temp = readFrom();
    		
    		if(temp != null)
    		{
    		    JSONParser parser = new JSONParser();
    		    
    			for(int i = 0; i < temp.length; i++)
    			{
    				String jsonString = temp[i];
    				//System.out.println("Rcv'd " + jsonString);
    				
    				// occasionally we receive null (incomplete) packets, usually
    				//     from disconnecting clients.
    				// in this case, just discard them and move on.
    				if(jsonString == null)
    					continue;
    				
    				// parse json strings here
    			    try
    			    {
						Object obj = parser.parse(jsonString);
						JSONObject obj2 = (JSONObject) obj;

						// read individual values as strings
						if(obj2.get("playerID") != null)
						{
							// this is player data; process it as such
							String username = obj2.get("username").toString();
							String posX_str = obj2.get("posX").toString();
							String posY_str = obj2.get("posY").toString();
							String charID_str = obj2.get("charID").toString();
							String facingRight_str = obj2.get("facingRight").toString();
							String playerID_str = obj2.get("playerID").toString();
							String kills_str = obj2.get("k").toString();
							String deaths_str = obj2.get("d").toString();
							
							// parse out raw values as needed
							float posX = Float.parseFloat(posX_str);
							float posY = Float.parseFloat(posY_str);
							int charID = Integer.parseInt(charID_str);
							boolean facingRight = Boolean.parseBoolean(facingRight_str);
							int playerID = Integer.parseInt(playerID_str);
							int kills = Integer.parseInt(kills_str);
							int deaths = Integer.parseInt(deaths_str);
							
							// debug: before changing our map, print its size
							//System.out.println("Map size: " + playerMap.size());
							
							// add to, or update, our playerMap
							if(playerMap.get(playerID) == null)
							{
								// we just received data for a player that doesn't exist on the server
								// --> create it
								NetworkedGamePlayer newPlayer = new NetworkedGamePlayer((int) posX, (int) posY, charID, 100, 100, facingRight);
								newPlayer.kills = kills;
								newPlayer.deaths = deaths;
								newPlayer.setUsername(username);
								playerMap.put(playerID, newPlayer);
							}
							else
							{
								// player already exists on server, just update it.
								NetworkedGamePlayer tmp = playerMap.get(playerID);
								tmp.posX = posX;
								tmp.posY = posY;
								tmp.facingRight = facingRight;
							}
						}
						else if(obj2.get("projectileID") != null)
						{
							// this is projectile data; process it as such
							String posX_str = obj2.get("posX").toString();
							String posY_str = obj2.get("posY").toString();
							String projectileID_str = obj2.get("projectileID").toString();
							String travelingRight_str = obj2.get("travelingRight").toString();
							String firedBy_str = obj2.get("firedBy").toString();
							
							// parse out raw values as needed
							float posX = Float.parseFloat(posX_str);
							float posY = Float.parseFloat(posY_str);
							int projectileID = Integer.parseInt(projectileID_str);
							boolean travelingRight = Boolean.parseBoolean(travelingRight_str);
							int firedBy = Integer.parseInt(firedBy_str);

							// create and store projectile data in our projectileMap
							NetworkedGameProjectile newProjectile = new NetworkedGameProjectile((int) posX, (int) posY, travelingRight, firedBy);
							newProjectile.timeFired = curTime;
							projectileMap.put(projectileID, newProjectile);
							
							// System.out.println("Stored projectile (current qty: " + projectileMap.size() + ")");
						}
						else if(obj2.get("chatMsg") != null)
						{
							// this is a chat message; queue it to be forwarded to all players
							queuedChatMessages.add(obj2);
						}
				    }
				    catch(ParseException | SlickException pe)
				    {
				        //System.out.println("position: " + pe.getPosition());
				        System.out.println(pe);
				    }
    			}
    		}
  
    		// is it time to send again yet?
    		if(curTime > timeLastSent + updateFrequency)
    		{
            	// It is, JSON-encode the game state
        		keys = playerMap.keySet();
        		
        		// loop over all players the server is aware of
    			JSONArray jsonData = new JSONArray();
        		for(Iterator<Integer> i = keys.iterator(); i.hasNext(); )
        		{
        			// package each player into a JSON object
    				JSONObject tmpObj = new JSONObject();
        		    int item = i.next();
        		    //System.out.println("packaging player " + item);
        		    
        		    NetworkedGamePlayer tmpPlayer = playerMap.get(item);

    				tmpObj.put("charID", new Integer( tmpPlayer.getCharID() ));
    				tmpObj.put("username", tmpPlayer.getUsername());
    				tmpObj.put("posX", new Float( tmpPlayer.getX() ));
    				tmpObj.put("posY", new Float( tmpPlayer.getY() ));
    				tmpObj.put("facingRight", new Boolean(tmpPlayer.facingRight));
    				tmpObj.put("playerID", new Integer(item));
    				tmpObj.put("k", new Integer(tmpPlayer.kills));
    				tmpObj.put("d", new Integer(tmpPlayer.deaths));
    				
    				jsonData.add(tmpObj);
        		}

        		// also package up data about all projectiles we know of
        		keys = projectileMap.keySet();
        		for(Iterator<Integer> i = keys.iterator(); i.hasNext(); )
        		{
        			// package each player into a JSON object
    				JSONObject tmpObj = new JSONObject();
        		    int item = i.next();
        		    //System.out.println("packaging projectile " + item);
        		    
        		    NetworkedGameProjectile tmpProj = projectileMap.get(item);

    				tmpObj.put("posX", new Float( tmpProj.getX() ));
    				tmpObj.put("posY", new Float( tmpProj.getY() ));
    				tmpObj.put("travelingRight", new Boolean(tmpProj.travelingRight));
    				tmpObj.put("projectileID", new Integer(item));
    				
    				jsonData.add(tmpObj);
        		}
        		
        		// send any death messages necessary
        		keys = queuedDeaths.keySet();
        		for(Iterator<Integer> i = keys.iterator(); i.hasNext(); )
        		{
        			int killed = i.next();
        			int killer = queuedDeaths.get(killed);
        			
    				JSONObject tmpObj = new JSONObject();
    				tmpObj.put("death", new Integer(killed));
    				tmpObj.put("by", new Integer(killer));
    				jsonData.add(tmpObj);
        		}
        		queuedDeaths.clear(); // all deaths have been sent, clear the queue
        		
        		// send any chat messages necessary
        		while(queuedChatMessages.size() > 0)
        		{
        			JSONObject tmpObj = new JSONObject();
        			String msg = queuedChatMessages.remove(0).get("chatMsg").toString();
        			tmpObj.put("chatMsg", msg);
        			System.out.println("Broadcasting a chat message to all players: " + msg);
        			jsonData.add(tmpObj);
        		}
        		
            	// write game state to JSocket
        		writeTo(String.valueOf(jsonData));
        		
        		timeLastSent = curTime;
    		}

    		processProjectiles();
    		
        	/*If number of players is 0, increment timeout*/
    		if(myPlayers.isEmpty())
    		{
    			if(!timeoutEnable)
    			{
    				timelimit = timeout + System.currentTimeMillis();
    				timeoutEnable = true;
    			}
    		}	
    		else
    		{
    			timeoutEnable = false;
    			timelimit = Long.MAX_VALUE;
    		}
    		
    		// keep this current
    		prevTime = curTime;
    	}
    	myHelper.closeMe();	//close helper
    	//Remove self from list if necessary
    	if(NetworkedGameServer.gameList.contains(this))
    		NetworkedGameServer.gameList.remove(this);
    }

	private void processProjectiles()
	{
		
		// "age" all projectiles an appropriate amount
		Set<Integer> keys = projectileMap.keySet();
		long delta = curTime - prevTime;
		for(Iterator<Integer> i = keys.iterator(); i.hasNext(); )
		{
		    int item = i.next();
			NetworkedGameProjectile projectile = projectileMap.get(item);
			
			if(projectile.travelingRight && projectile.moveX(0.5f * delta))
			{
				i.remove(); // we had a collision, remove this projectile
			}
			else if(!projectile.travelingRight && projectile.moveX(-0.5f * delta)) // traveling left
			{
				i.remove(); // we had a collision, remove this projectile
			}
			else if(curTime > projectile.timeFired + projectile.LIFESPAN)
			{
				i.remove(); // projectile has expired, kill it
			}
			else
			{
				// projectile is still valid. check for collisions with players
				Set<Integer> playerKeys = playerMap.keySet();
				for(Iterator<Integer> p = playerKeys.iterator(); p.hasNext(); )
				{
					// create a bounding box representing each player
					int whichPlayer = p.next();
					NetworkedGamePlayer player = playerMap.get(whichPlayer);
					Rectangle rect = new Rectangle(player.getX(), player.getY(),
							player.getWidth(), player.getHeight());

					// see if the projectile intersects it
					if(rect.contains(projectile.getX(), projectile.getY()))
					{
						// ensure a player doesn't shoot themselves
						if(whichPlayer != projectile.firedBy && !player.isDead())
						{
							// hey look at that, we had a death
							i.remove();
							int killerID = projectile.firedBy;
							if(playerMap.get(killerID) != null)
								playerMap.get(killerID).kills++; // log the kill if relevant

							player.deaths++;
							player.setDeathState(true);
							System.out.println("Someone died");
							
							// queue up a death message
							queuedDeaths.put(whichPlayer, killerID);
						}
					}
				}
			}
		}
	}
	
} // end NetworkedGameServer class

/**
 * @author Ben
 *	A helper thread for the gamethread to listen to clients connecting this particular game
 */
class NetworkedGameServerThreadHelper extends Thread
{
	private ServerSocket serverSocket = null;
	private int portNumber;
	private boolean listening = true;
	NetworkedGameServerThread master;
	public NetworkedGameServerThreadHelper(int portNumber, NetworkedGameServerThread master)
	{
		this.portNumber = portNumber;
		this.master = master;
	}
	
	public void run()
	{
		try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
			serverSocket.setReuseAddress(true);
        	while (listening)
        	{
        		PlayerThread newPlayer = new PlayerThread(serverSocket.accept(), master);
        		newPlayer.start();
        		master.myPlayers.add(newPlayer);
        		System.out.println("New player added. Pop is now " + master.population());												//TEST OUTPUT
        	}
        	
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            return;
	    }
	}
	
	void closeMe()
	{
		listening = false;
	}
}


/**
 * @author Ben
 * A helper thread that cleans up mapping relationships of any object to its thread
 * pre:	P has an ID that can be mapped to O
 */
class ThreadMapThreadCleanUp<T extends NetworkedGameServerThread, P extends PlayerThread, O extends Object> extends Thread
{
	T master;													//Master
	private volatile Map<Integer, O> masterMap;					//master's map
	private int sleepDuration = 3000;							//How long I sleep before I wake up and clean it again
	private boolean hasAccess = false;							//I am accessing this block
	
	/**
	 * @param myMap:	Pass in a volatile copy of your map, so I can clean it up for you.
	 */
	public ThreadMapThreadCleanUp(T master, Map<Integer, O> masterMap)
	{
		super("ThreadMapThreadCleanUp");
		this.master = master;
		this.masterMap = masterMap;
	}
	
	public void run()
	{
		hasAccess = true;
		
		ArrayList<Integer> currentIDs = new ArrayList<Integer>();
		//While master is still running
		while(master.getState() != Thread.State.TERMINATED)
		{
			//Check for each object that it's thread isn't dead. If dead, remove the object
			for(Iterator<PlayerThread> i = master.myPlayers.iterator(); i.hasNext();)
	    	{
	    		currentIDs.add(i.next().getClientID());
	    	}
			
			//System.out.println("Periodic check, Number of players: " + currentIDs.size());						//Debug output
			
			try {
				for (Map.Entry<Integer, O> entry : masterMap.entrySet())
				{
					//Remove if not found among master's player threads
				    if(!currentIDs.contains(entry) && !master.markedForRemove.contains(entry))
				    {
				    	master.markedForRemove.add(entry.getKey());
				    }
				}
			} catch(ConcurrentModificationException e)	{
				e.printStackTrace();
			}
			
			//System.out.println("Periodic check, Number of players: " + masterMap.size());						//Debug output*/
			currentIDs.clear();
			hasAccess = false;
			
			
			//Sleep for a while
			try {
				sleep(sleepDuration);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	/**
	 * @return Whether or not this class holds access to its volatile Map resource
	 */
	public boolean hasMapAccess()
	{
		return hasAccess;
	}
}