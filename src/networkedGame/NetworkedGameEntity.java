package networkedGame;

import org.newdawn.slick.SlickException;
import networkedGame.NetworkedGameData;

public class NetworkedGameEntity
{
	protected float posX, posY;
	protected float posOldX, posOldY;
	protected int width, height;
	
	// items pulled from data class
	private static final int TILE_SIZE = NetworkedGameData.TILE_SIZE;
	private static final int BLOCK_SOLID = NetworkedGameData.BLOCK_SOLID;
	private static int tileType[][] = NetworkedGameData.tileType;

	public NetworkedGameEntity() throws SlickException {
		this(100f, 100f);
	}

	public NetworkedGameEntity(int x, int y) throws SlickException {
		this((float) x, (float) y, 100, 100);
	}

	public NetworkedGameEntity(float x, float y) throws SlickException {
		this(x, y, 100, 100);
	}

	public NetworkedGameEntity(int x, int y, int w, int h) throws SlickException {
		this((float) x, (float) y, w, h);
	}

	public NetworkedGameEntity(float x, float y, int w, int h) throws SlickException {
		posX = x;
		posY = y;
		posOldX = posX;
		posOldY = posY;
		width = w;
		height = h;
	}

	/**
	 * Attempt to move entity's X coordinate by the specified amount.
	 * @param delta
	 * @return True if collision occurred; false otherwise.
	 */
	public boolean moveX(float delta) {
		posX += delta;
		
		// check left corners for collision. move back just as far as needed, if needed
		for(int pos = 0; pos < height; pos++)
		{
			if(checkCollision(posX, posY+pos) == BLOCK_SOLID) {
				posX = ((int) (posX/TILE_SIZE) + 1) * TILE_SIZE;
				return true;
			}
		}
		// check right corners for collision. move back just as far as needed, if needed
		for(int pos = 0; pos < height; pos++)
		{
			if(checkCollision(posX+width, posY+pos) == BLOCK_SOLID) {
				posX = ((int) ((posX+width)/TILE_SIZE)) * TILE_SIZE - width;
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Attempt to move entity's Y coordinate by the specified amount.
	 * @param delta
	 * @return True if collision occurred; false otherwise.
	 */
	public boolean moveY(float delta) throws SlickException {
		posY += delta;
		
		// check bottom corners for collision. move back just as far as needed, if needed
		for(int pos = 0; pos < width; pos++)
		{
			if(checkCollision(posX+pos, posY+height) == BLOCK_SOLID) {
				posY = ((int) ((posY+height)/TILE_SIZE)) * TILE_SIZE - height;
				NetworkedGameMain.stopJump(true);
				return true; // only need to find 1 collision
			}
		}
		
		// check top corners for collision. move back just as far as needed, if needed
		for(int pos = 0; pos < width; pos++)
		{
			if(checkCollision(posX+pos, posY) == BLOCK_SOLID) {
				posY = ((int) (posY/TILE_SIZE) + 1) * TILE_SIZE;
				NetworkedGameMain.stopJump(false);
				return true;
			}
		}
		
		return false;
	}
	
	// returns true if a collision is occurring at the given x, y
	public int checkCollision(float x, float y)
	{
		int tileX = (int) (x/TILE_SIZE);
		int tileY = (int) (y/TILE_SIZE);
		
		//System.out.println("tileX = " + tileX + "tileY = " + tileY + "lenY = " + tileType.length + "lenY = " + tileType[0].length);
		
		if(tileType == null || tileType[0] == null)
		{
			System.out.println("!!! tileType issue");
			return BLOCK_SOLID; // hold it there!!
		}
		else if(tileX > 0 && tileY > 0 && tileX < tileType.length && tileY < tileType[0].length)
			return tileType[tileX][tileY];
		else
			return BLOCK_SOLID; // hold it there!!
	}
	
	// posOldX & Y get assigned current (valid) posX & Y values
	public void tick() {
		posOldX = posX;
		posOldY = posY;
	}
	
	// ---  Getters  ---
	public float getX() {
		return posX;
	}
	public float getY() {
		return posY;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
	// ---  Setters  ---
	public void setX(float pos) {
		posX = pos;
		posOldX = pos;
	}
	public void setY(float pos) {
		posY = pos;
		posOldY = pos;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public void setPos(float x, float y) {
		setX(x);
		setY(y);
	}
}
