package networkedGame;

import org.newdawn.slick.SlickException;

public class NetworkedGamePlayer extends NetworkedGameEntity
{
	protected int charID;
	protected int kills = 0;
	protected int deaths = 0;
	private String userName = "";
	protected boolean facingRight = true;
	protected boolean dead = false;

	public NetworkedGamePlayer() throws SlickException {
		super();
	}

	public NetworkedGamePlayer(int x, int y) throws SlickException {
		super(x, y);
	}

	public NetworkedGamePlayer(int x, int y, int charID) throws SlickException {
		this(x, y, charID, true);
	}

	public NetworkedGamePlayer(int x, int y, int charID, boolean right) throws SlickException {
		super(x, y);
		this.charID = charID;
		this.facingRight = right;
	}

	public NetworkedGamePlayer(int x, int y, int charID, int w, int h, boolean right) throws SlickException {
		super(x, y, w, h);
		this.charID = charID;
		this.facingRight = right;
	}
	
	public String getUsername()
	{
		return userName;
	}
	
	public void setUsername(String username)
	{
		if(username != null)
			this.userName = username;
	}

	public int getCharID() {
		return charID;
	}
	
	public boolean isFacingRight() {
		return facingRight;
	}
	
	public void setFacingRight(boolean value) {
		facingRight = value;
	}
	
	public int getKills()
	{
		return kills;
	}
	
	public int getDeaths()
	{
		return deaths;
	}
	
	public void setDeathState(boolean state)
	{
		dead = state;
	}
	
	public boolean isDead()
	{
		return dead;
	}
}
