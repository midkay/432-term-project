package networkedGame;

import java.net.*;
import java.io.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.ArrayList;

import org.newdawn.slick.SlickException;

public class NetworkedGameServer
{	
	private static NetworkedGameData gameData;
	private static final int capacity = 100000;				//capacity of userList
	private static final int maxGames = 40;					//maximum games available
	private static NetworkedGameServer instance;			//singleton of the gameserver, you can only have 1
	private static final int[] gameChannelPorts = { 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 
		20000, 21000, 22000, 21000, 22000, 23000, 24000, 25000 };		//the available ports for game channels
	static final int port1 = 2780;				//Channel 1 is for hotjoining, that is, it's run on this file's main()
	
	static NetworkedGameServerThread Channel1 = null;
	static BlockingQueue<String> userList = new ArrayBlockingQueue<String>(capacity);										//list of who is logged in
	static BlockingQueue<NetworkedGameServerThread> gameList = new LinkedBlockingQueue<NetworkedGameServerThread>(maxGames);//shared game list
	
	public static void main(String[] args) throws IOException, SlickException
	{
		// This is channel 1, and can be hot joined
		Channel1 = new NetworkedGameServerThread(port1, "Channel 1", 150, 3600000);	//Make Channel 1
		Channel1.start();
		
		/* for(int i = 2; i < 10; i++)
		{
			NetworkedGameServerThread ChannelN = new NetworkedGameServerThread(gameChannelPorts[i], "Channel " + i, 150, 3600000);	//Make Channel N
			ChannelN.start();
			gameList.add(ChannelN);
		} */
		
		// Turn on other processes
        int portNumber = 10668; //Integer.parseInt(args[0]);
        boolean listening = true;
        
        // attempt to load map data
        loadMapDataFromFile();
         
        try (ServerSocket serverSocket = new ServerSocket(portNumber))
        {
        	while (listening)
        	{
        		new NetworkedGameListener(serverSocket.accept(), instance, userList).start();
        	}
        	
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
	    }
	}
	
	// Load int[][] tileType array from file
	private static void loadMapDataFromFile() throws IOException, SlickException
	{
    	String dataFile = NetworkedGameData.TILE_DATA_LOCATION;
    	int[][] tileType = null;

		System.out.println("Loading map data from " + dataFile + "...");
		
		// read in directly as an object (it was written to file as such)
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(dataFile));
		try {
			tileType = (int[][]) inputStream.readObject();
			System.out.println("Loaded. 2D array = " + tileType.length + " x " + tileType[0].length);
			gameData = new NetworkedGameData(tileType);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		inputStream.close();
	}

	/**
	 * @param none, it just shows list of players now as debug
	 * @return none, void
	 */
	static synchronized void debugPlayerList()
	{
		System.out.println("List of players: ");
		Object[] playerListToShow = userList.toArray();
		for(int i = 0; i < playerListToShow.length; i++)
		{
			System.out.println(playerListToShow[i]);
		}
	}
	
	/**
	 * Run a new game
	 * @return port#, the next available port# we can open a gamethread. -1 if fail
	 * //REQ.3: Create a game
	 */
	static synchronized int	startGame()
	{
		//If full, we cannot add games, return -1
		if(gameList.remainingCapacity() == 0)
			return -1;
		
		ArrayList<Integer> usedPorts = new ArrayList<Integer>();
		NetworkedGameServerThread[] mGames = (NetworkedGameServerThread[]) gameList.toArray();
		for(int i = 0; i < mGames.length; i++)
		{
			usedPorts.add(mGames[i].getPortNumber());
		}
		for(int i = 0; i < maxGames; i++)
		{
			if(!usedPorts.contains(gameChannelPorts[i]))
			{
				NetworkedGameServerThread ChannelN = new NetworkedGameServerThread(gameChannelPorts[i], "Channel");	//Make Channel 1
				ChannelN.start();
				gameList.add(ChannelN);
				return gameChannelPorts[i];
			}
		}
		return -1;
	}
}
