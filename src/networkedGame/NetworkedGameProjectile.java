package networkedGame;

import org.newdawn.slick.SlickException;

public class NetworkedGameProjectile extends NetworkedGameEntity
{
	public static final int width = 34;
	public static final int height = 10;
	public static final int LIFESPAN = 1117;
	public boolean travelingRight = true;
	public long timeFired = 0;
	public int firedBy;
	
	public NetworkedGameProjectile() throws SlickException
	{
		this(100, 100, true); // random x/y pos
	}
	
	public NetworkedGameProjectile(float x, float y, boolean right) throws SlickException
	{
		this(x, y, right, -1);
	}

	public NetworkedGameProjectile(float x, float y, boolean right, int firedBy) throws SlickException
	{
		super(x, y, width, height);
		travelingRight = right;
		this.firedBy = firedBy;
	}
	
}
