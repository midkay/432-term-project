package networkedGame;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.tiled.TiledMap;

public class NetworkedGameData
{
	// ---  Constants  ---
	protected static final String RES_LOCATION = "res/";
	protected static final String MAP_LOCATION = "bigmap2.tmx";
	protected static final String TILE_DATA_LOCATION = RES_LOCATION +
			MAP_LOCATION.substring(0,  MAP_LOCATION.indexOf(".")) + "_tiledata.txt";

	protected static final int NUM_LASERS = 7;
	
	// ---  Data structures  ---
	// Note: The following array stores partial filenames of character images.
	// You may simply add one, but ensure in resLocation there are _right.png
	// and _left.png images corresponding to it (brent_left.png, brent_right.png)
	protected static String[] characters = { "brent", "olson", "hazel", "wooyoung",
											 "carol", "kelvin", "dimpsey", "jeffy", "min" };
	protected static String[] characterProperNames = { "Brent", "Olson", "Hazeline",
							  		"Wooyoung", "Zander", "Kelvin", "Dimpsey", "Jeff Kim", "Min Chen" };
	protected static ArrayList<Image> charFacingLeftList;
	protected static ArrayList<Image> charFacingRightList;
	protected static ArrayList<Image> laserList;
	
	// ---  Map-related  ---
	protected static TiledMap map = null;
	protected static int[][] tileType = null;
    protected static ArrayList<Rectangle> objectList = null;
	public static final int TILE_SIZE = 32;
	static final int BLOCK_SOLID = 1;
	
    // default constructor attempts to build map data from map file itself
    // note that this will throw an exception if not being called from within a slick game
    public NetworkedGameData() throws SlickException
    {
    	this(null);
    	
    	objectList = new ArrayList<Rectangle>();
		charFacingLeftList = new ArrayList<Image>();
		charFacingRightList = new ArrayList<Image>();
		laserList = new ArrayList<Image>();

		try {
			// must trim trailing slash off resLocation for this function call
			map = new TiledMap(RES_LOCATION + MAP_LOCATION, RES_LOCATION.substring(0, RES_LOCATION.length()-1));
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// load collision tile properties into 2D boolean array for faster access
		tileType = new int[map.getWidth()][map.getHeight()];
		for(int layer = 0; layer < map.getLayerCount(); layer++)
		{
			for (int i = 0; i < map.getWidth(); i++)
			{
				for (int j = 0; j < map.getHeight(); j++)
				{
					int tileID = map.getTileId(i, j, layer);
					
					// 'collide' tiles, on only the world layer, are considered solid blocks
					if(layer == map.getLayerIndex("world") && "true".equals(map.getTileProperty(tileID, "collide", "false")))
					{
						tileType[i][j] = BLOCK_SOLID;
						objectList.add(new Rectangle(i*TILE_SIZE, j*TILE_SIZE, TILE_SIZE, TILE_SIZE));
					}
				}
			}
		}
		
		// store parsed data locally for server to use
		try {
			writeMapDataToFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// initialize character sprite ArrayLists
		for(int i = 0; i < characters.length; i++)
		{
			Image tmp = new Image(RES_LOCATION + characters[i] + "_left.png");
			charFacingLeftList.add(tmp);

			tmp = new Image(RES_LOCATION + characters[i] + "_right.png");
			charFacingRightList.add(tmp);
		}
		
		// initialize laser sprites
		for(int i = 0; i < NUM_LASERS; i++)
		{
			Image tmp = new Image(RES_LOCATION + "laser" + i + ".png");
			laserList.add(tmp);
		}
    }
    
    // secondary constructor giving a minimal gameData class, by letting you
    // manually specify tile set data. this is used by the server
    public NetworkedGameData(int[][] tiles) throws SlickException
    {
		tileType = tiles;
    }
    
    public void loadMapData()
    {
    	
    }
    
    private void writeMapDataToFile() throws IOException
    {
    	ObjectOutputStream outputStream;
    	outputStream = new ObjectOutputStream(new FileOutputStream(TILE_DATA_LOCATION));
    	outputStream.writeObject(tileType);
    	outputStream.flush();
    	outputStream.close();
    }
    
    public int getNumberOfCharacterTypes()
    {
    	return characters.length;
    }
    
    public Image getCharFacingRight(int charID)
    {
    	return charFacingRightList.get(charID);
    }
    
    public Image getCharFacingLeft(int charID)
    {
    	return charFacingLeftList.get(charID);
    }
    
    public String getCharName(int charID)
    {
    	if(charID < characterProperNames.length)
    		return characterProperNames[charID];
    	else
    		return "";
    }
    
    public int getCharHeight(int charID)
    {
    	return getCharFacingLeft(charID).getHeight();
    }

    public int getCharWidth(int charID)
    {
    	return getCharFacingLeft(charID).getWidth();
    }

    public int getNumLaserTypes()
    {
    	return NUM_LASERS;
    }

    public Image getLaser(int laserID)
    {
    	if(laserID < laserList.size())
    		return laserList.get(laserID);
    	else
    		return null;
    }
    
	// returns true if a collision is occurring at the given x, y
	public int checkCollision(float x, float y)
	{
		int tileX = (int) (x/TILE_SIZE);
		int tileY = (int) (y/TILE_SIZE);
		
		//System.out.println("tileX = " + tileX + "tileY = " + tileY + "lenY = " + tileType.length + "lenY = " + tileType[0].length);
		
		if(tileType == null || tileType[0] == null)
		{
			System.out.println("!!! tileType issue");
			return BLOCK_SOLID; // hold it there!!
		}
		else if(tileX > 0 && tileY > 0 && tileX < tileType.length && tileY < tileType[0].length)
			return tileType[tileX][tileY];
		else
			return BLOCK_SOLID; // hold it there!!
	}
}
