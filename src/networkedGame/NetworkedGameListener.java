package networkedGame;

import java.net.*;
import java.io.*;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class NetworkedGameListener extends Thread {
	private Socket socket = null;
	private BlockingQueue<String> q;
	private NetworkedGameServer master;
	
	private static final int LOGINSCREEN = -1;
	private static final int LOBBYSCREEN = 1000;
	private int state = LOGINSCREEN;
	private String mUserName = "";
	
	// --- Stuff to talk to client with
	private static String listOfGames;						//Cache the game list
	private static long expiration = 0;						//Track TTL here
	private static long timeToLive = 10000;					//Time in milliseconds before the cached list becomes stale.
	private static long createRoomCoolDown = 1000;			//Server can only spawn a game room per this cycle, to prevent flooded requests
	private static long createRoomCoolDownTimer = 0;		//Track the create room cooldown here
	private String[] ClientToServerRolePlayLines = {"REQUEST_LIST_OF_GAMES", "RESERVE_THIS_USERNAME", "RESERVE_THIS_ROOM"};			//The lines I use to talk to the server
	private String ServerDelimit = ";;";																	//Shortcut delimiter for quick structured access
	private String[] ServerToClientRolePlayLines = {"ACK", "NAK"};																//The lines I listen from server for
	private static final int REQUEST_LIST_OF_GAMES = 0, RESERVE_THIS_USERNAME = 1, RESERVE_THIS_ROOM = 2 ,INVALID_USER_REQUEST = -1;
	
	
	/*
	//private static final String closeConnection = "\r\n\r\n";
	//private static final String[] outputLine = { "Enter a user name: ",
			//"Name taken. Enter different user name: "};
	//private static final String[] lobbyLine = { "Enter the number for the game lobby you want to join, or 'C' to start a game, or 'Q' to unregister.",
		//"List of Current Games: " };*/
	
	public NetworkedGameListener(Socket socket, NetworkedGameServer master, BlockingQueue<String> q) {
        super("NetworkedGameListener");
        this.socket = socket;
        this.q = q;
        this.master = master;
    }
	
	public void run() {
		try {
			socket.setTcpNoDelay(true);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try (
	            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	            BufferedReader in = new BufferedReader(
	                new InputStreamReader(
	                    socket.getInputStream()));
		   )
		   {
				int oIter = 0;					//The output iterator
				String inputLine, outputLine;
				
				
				/*READ the user input and DETERMINE which state*/
				inputLine = in.readLine();
				System.out.println(inputLine);																//Debug output
				
				if(inputLine.equals(ClientToServerRolePlayLines[0]))		//Requested Game List
				{
					state = REQUEST_LIST_OF_GAMES;
					outputLine = getCurrentGames();
					out.println(outputLine);
					//System.out.println(outputLine);																//Debug Output
				}
				else if(inputLine.equals(ClientToServerRolePlayLines[1]))		//Requested Name Reservation
				{
					state = RESERVE_THIS_USERNAME;
				}
				else if(inputLine.equals(ClientToServerRolePlayLines[2]))		//Requested Name Reservation
				{
					state = RESERVE_THIS_USERNAME;
					String roomName = "Private Room";
					int portNum = -1;
					
					/*GET reservation from client*/
					try {
						if((inputLine = in.readLine()) != null)
							portNum = Integer.parseInt(inputLine);
						if((inputLine = in.readLine()) != null)
							roomName = inputLine;
						//System.out.println("Attempting to open \'" + roomName + "\' on port " + portNum);		//Debug Output
						
						/*ATTEMPT reservation and return result*/
						boolean success = NetworkedGameListener.reserveRoom(roomName, portNum);
						if(success)
							out.println(ServerToClientRolePlayLines[0]);		//ACK
						else
							out.println(ServerToClientRolePlayLines[1]);		//NAK
					} catch (NumberFormatException | IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					} catch (Exception e)
					{
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
				}
				else		//By default, user request is invalid
				{
					System.err.println("Invalid Request: " + inputLine);										//Debug Output
					state = INVALID_USER_REQUEST;
				}
				
				/*FINISH the transaction*/
				socket.close();					//User has quit game
				return;							//kill thread
		   }
		   catch (IOException e) {
		            e.printStackTrace();
		   }
	}
	
	/**
	 * Get a list of games on the server
	 */
	private String getCurrentGames()
	{
		long curTime = System.currentTimeMillis();
		String mGames = null;
		
		/*CHECK if my cache needs to be updated*/
		if(curTime > expiration)
		{
			mGames = NetworkedGameServer.Channel1.status();
			mGames += ServerDelimit;
			
			Iterator<NetworkedGameServerThread> i = NetworkedGameServer.gameList.iterator();
			while(i.hasNext())
			{
				mGames += i.next().status() + ServerDelimit;
			}
			
			expiration = System.currentTimeMillis() + timeToLive;
			listOfGames = mGames;
		}
		/*Return copy of my cache*/
		else
		{
			mGames = listOfGames;
		}
		
		return mGames;
	}
	
	/**
	 * Unregister the user from the list of players
	 */
	private void unregisterPlayer()
	{
		try {
			NetworkedGameServer.userList.remove(mUserName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * A single synchronized method that attempts to make a game
	 * @param name			:	Name of this room to reserve
	 * @param portNumber	:	Port number to reserve this room in
	 * @return		True if a game can be hosted, else false.
	 */
	private static synchronized boolean reserveRoom(String name, int portNumber)
	{
		long curTime = System.currentTimeMillis();
		
		/*Check if this ability has cooled down, false if not*/
		if(curTime < createRoomCoolDownTimer)
			return false;
		//Block attempts to create a port lower than minPortNumber and port1
		if(portNumber < NetworkedGameServerThread.minPortNumber || portNumber == NetworkedGameServer.port1)
			return false;
		//Cannot add a new game, full
		if(NetworkedGameServer.gameList.remainingCapacity() <= 0)
			return false;
		
		/*CHECK to see if a game is already is using this port*/
		Iterator<NetworkedGameServerThread> i = NetworkedGameServer.gameList.iterator();
		while(i.hasNext())
		{
			if(i.next().getPortNumber() == portNumber)
				return false;
		}
		NetworkedGameServerThread newRoom = new NetworkedGameServerThread(portNumber, name, 100, 60000);
		newRoom.start();
		if(newRoom.isAlive())
		{
			NetworkedGameServer.gameList.add(newRoom);
			//System.out.println("Remaining capacity: " + NetworkedGameServer.gameList.remainingCapacity());				//Debug output
			return true;
		}
		return false;
	}
}
