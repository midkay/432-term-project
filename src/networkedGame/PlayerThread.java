package networkedGame;

import java.net.*;
import java.util.concurrent.BlockingQueue;
import java.io.*;

//import java.util.concurrent.ArrayBlockingQueue;
//import java.util.concurrent.BlockingQueue;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PlayerThread extends Thread {
	private Socket socket = null;
	private String mUserName = "";
	private NetworkedGameServerThread master;
	private boolean wait = true;
	private int timeoutThreshold = 1;			//Max socket exceptions you can have in a row
	private int timeoutCounter = 0;				//counter for that, resetting on receiving actual data
	private int faultThreshold = 1000;			//Maximum concurrent faults, due to disconnecting client and me getting exceptions on in.readLine()
	private int faultCounter = 0;				//counter for that
	private int sleepDelay = 30;				//Sleep this long to reduce conflicting attempts to access to BlockingQueues. 
	
	/*CLIENT INFO*/
	private String clientName;
	private int clientID;
	
	/**
	 * @param socket		Socket to communicate with client, given by the game thread
	 * @param master		The game thread master to this player thread, because it passes itself to the constructor
	 * @param serverData	JSON block we read from to push to client to decode
	 * @param clientData	JSON block we write to for the game server to read from
	 */
	public PlayerThread(Socket socket, NetworkedGameServerThread master, BlockingQueue<String> serverData, BlockingQueue<String> clientData)
	{
		super("PlayerThread");
		this.socket = socket;
		this.master = master;
	}
	
	/**
	 * @param socket		Socket to communicate with client, given by the game thread
	 * @param master		The game thread master to this player thread, because it passes itself to the constructor
	 * @param serverData	JSON block we read from to push to client to decode
	 * @param clientData	JSON block we write to for the game server to read from
	 */
	public PlayerThread(Socket socket, NetworkedGameServerThread master)
	{
		super("PlayerThread");
		this.socket = socket;
		this.master = master;
	}
	
	public void run() 
	{
		String input;
		String output;
		
		try (
	            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	            BufferedReader in = new BufferedReader(
	                new InputStreamReader(
	                    socket.getInputStream()));
		   )
		   {
			long timelimit = Long.MAX_VALUE;
			
			//Initiate player data, if can't then give up
			if(!initiatePlayer(in, out))
			{
				socket.close();
				return;
			}
			System.err.println("Session ID for " + getClientName() +" is " + getClientID());																//Console Logging
			while(true)
			{
				try {
					Thread.sleep(sleepDelay);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Get gamethread's JSON string and push it into socket to client.
				output = master.gameData.peek();
				if(output != null)
				{
					out.println(output);
					//System.out.println(output);																	//TEST OUTPUT
				}
				
				//if(poll from client) Read from client and push it to buffer for gamethread to read from
				try {
					input = in.readLine();					//Client must write to server to unblock
					if(input != null)
					{
						master.clientData.add(input);		//add input into clientData buffer
						//System.out.println(input);																//TEST OUTPUT
						timeoutCounter = 0;
					}
				} 
				catch (SocketTimeoutException e)
				{
					timeoutCounter++;
					if(timeoutCounter >= timeoutThreshold)
					{
						master.myPlayers.remove(this);
						System.err.println("Kicked due to inactivity. Pop is now " + master.population());							//Console Logging
						return;
					}
					//Increment counter. When it's too high, assume client is disconnected
				}catch (Exception e) {
					faultCounter++;		//Increment counter. When it's too high, assume client is disconnected
					if(faultCounter >= faultThreshold)
					{
						master.myPlayers.remove(this);
						System.err.println("Kicked due to disconnect. Pop is now " + master.population());							//Console Logging
						return;
					}
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				
				//System.out.println("LOOP");																//TEST OUTPUT
			}
		   }
		   catch (IOException e) {
	            e.printStackTrace();
	   }
	}
	
	/**
	 * Initializes data about the player and saves it to this instance of player thread
	 * @param in
	 * @param out
	 */
	boolean initiatePlayer(BufferedReader in, PrintWriter out)
	{
		String input = null;
		try {
			/*Get initial values from Client*/
			int threshold = 100;
			int timeoutCounter = 0;
			//System.out.println("Good");																	//Debug output
			while(input == null)
			{
				try {
					input = in.readLine();
					sleep(sleepDelay);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SocketTimeoutException e) {
					//Nothing bad
				}
				timeoutCounter++;
				if(timeoutCounter > threshold)
				{
					return false;
				}
			}
			//System.out.println("Good");																	//Debug output
			if(input != null)
			{
				//JSON decode
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(input);
				JSONObject obj2 = (JSONObject) obj;
				
				//Check and parse JSON fields
				if(obj2.get("playerID") == null || obj2.get("username") == null)
				{
					return false;
				}
				else
				{
					clientName = obj2.get("username").toString();
					String charID_str = obj2.get("playerID").toString();
					clientID = Integer.parseInt(charID_str);
					
					/*Check input fields*/
				}
				//System.out.println("Good");																	//Debug output, #3 in this loop
				return true;
			}
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Sets wait to false, enabling the server to read from gamethread's broadcast and forward it.
	 */
	public void enable()
	{
		wait = false;
	}
	
	public String getClientName()
	{
		return clientName;
	}
	
	public int getClientID()
	{
		return clientID;
	}
}
